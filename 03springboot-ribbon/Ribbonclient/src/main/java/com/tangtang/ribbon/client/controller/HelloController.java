package com.tangtang.ribbon.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@RequestMapping("hi")
@RestController
public class HelloController {
    @Autowired
    private RestTemplate restTemplate;


    // 标注此注解后，RestTemplate就具有了客户端负载均衡能力
    @LoadBalanced
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @GetMapping("to")
    public String hi(String name) {
        //下面地址 写全带http 否则会提示请求地址有问题
        return restTemplate.getForObject("http://ribbon-provider/hello/to/" + name, String.class);
    }
}
