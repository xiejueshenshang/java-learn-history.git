package com.tangtang.ribbon.provider.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("hello")
public class HelloController {
    @GetMapping("to/{name}")
    public String hello(@PathVariable String name){
        System.out.println("服务被调用"+"hello " + name);
        return "服务被调用"+"hello " + name;
    }
}
