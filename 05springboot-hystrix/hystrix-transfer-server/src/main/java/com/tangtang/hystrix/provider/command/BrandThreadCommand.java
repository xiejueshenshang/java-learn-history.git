package com.tangtang.hystrix.provider.command;

import com.alibaba.fastjson.JSONObject;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandKey;
import com.netflix.hystrix.HystrixCommandProperties;
import com.netflix.hystrix.HystrixObservableCommand;
import com.netflix.hystrix.HystrixRequestCache;
import com.netflix.hystrix.strategy.concurrency.HystrixConcurrencyStrategyDefault;
import com.tangtang.hystrix.provider.utils.HttpClientUtils;
import com.tangtang.hystrix.provider.vo.BrandVO;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

public class BrandThreadCommand extends HystrixObservableCommand<BrandVO> {

    private static final HystrixCommandKey GETTER_KEY = HystrixCommandKey.Factory.asKey("OrderBrand");

    private Long id;

//    private final Setter cacheSetter = Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("orderGroup"))
//            .andCommandKey(GETTER_KEY);

    public BrandThreadCommand(Long id){
        //设置设置线程池的方式进行请求
        super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("brandGroup"))
                .andCommandKey(GETTER_KEY)
                .andCommandPropertiesDefaults(HystrixCommandProperties.Setter()
                        .withExecutionIsolationStrategy(HystrixCommandProperties.ExecutionIsolationStrategy.THREAD)));


//        HystrixCommand.Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("ProductGroup"))
//                .andCommandKey(HystrixCommandKey.Factory.asKey("GetProductInfoCommand"))
//                .andThreadPoolKey(HystrixThreadPoolKey.Factory.asKey("GetProductInfoPool"))
//                .andThreadPoolPropertiesDefaults(HystrixThreadPoolProperties.Setter()
//                        .withCoreSize(15)
//                        .withMaxQueueSize(10)
//                        .withQueueSizeRejectionThreshold(10))
//                .andCommandPropertiesDefaults(HystrixCommandProperties.Setter()
//                        .withCircuitBreakerRequestVolumeThreshold(30)
//                        .withCircuitBreakerErrorThresholdPercentage(40)
//                        .withCircuitBreakerSleepWindowInMilliseconds(3000)
//                        .withExecutionTimeoutInMilliseconds(500)
//                        .withFallbackIsolationSemaphoreMaxConcurrentRequests(30));
        this.id = id;
    }

    @Override
    protected Observable<BrandVO> construct() {
        return Observable.unsafeCreate(new Observable.OnSubscribe<BrandVO>() {
            @Override
            public void call(Subscriber<? super BrandVO> observer) {
                try {
                    if (!observer.isUnsubscribed()) {
                        String request = HttpClientUtils.sendGetRequest("http://127.0.0.1:8086/api/v1/brand/"+id);
                        BrandVO brandVO  =  JSONObject.parseObject(request, BrandVO.class);
                        observer.onNext(brandVO);
                        observer.onCompleted();
                    }
                } catch (Exception e) {
                    observer.onError(e);
                }
            }
        } ).subscribeOn(Schedulers.io());
    }

    @Override
    protected String getCacheKey() {
        return String.valueOf(id);
    }

    /**
     * Allow the cache to be flushed for this object.
     *
     * @param id
     *            argument that would normally be passed to the command
     */
    public static void flushCache(int id) {
        HystrixRequestCache.getInstance(GETTER_KEY,
                HystrixConcurrencyStrategyDefault.getInstance()).clear(String.valueOf(id));
    }


}
