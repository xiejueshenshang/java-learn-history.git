package com.tangtang.hystrix.provider.controller;

import com.tangtang.hystrix.provider.client.BrandClient;
import com.tangtang.hystrix.provider.vo.BrandVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/v1/test/brand")
@RestController
public class BrandTestController {
    @Autowired
    private BrandClient brandClient;

    @GetMapping("{id}")
    public BrandVO getBrand(@PathVariable Long id){
        return brandClient.getBrand(id);
    }
}
