package com.tangtang.hystrix.provider.command;

import com.alibaba.fastjson.JSONObject;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.tangtang.hystrix.provider.utils.HttpClientUtils;
import com.tangtang.hystrix.provider.vo.ProductVO;

import java.math.BigDecimal;

public class TestFallBackCommand extends HystrixCommand<ProductVO> {


    private Long id;
    public TestFallBackCommand(Long id){
        super(HystrixCommandGroupKey.Factory.asKey("ProductGroup"));
        this.id = id;
    }

    @Override
    protected ProductVO run() throws Exception {
        throw new RuntimeException("调用TestFallBackCommand失败");
    }



    @Override
    protected ProductVO getFallback() {
        return new ProductVO(99L, "defultName", BigDecimal.ZERO,
                "defaultAddress", 0L, null);
    }
}
