package com.tangtang.hystrix.provider.client;

import com.tangtang.hystrix.provider.vo.BrandVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "hystrix-provide",fallback = BrandClient.BrandFallBack.class)
public interface BrandClient {
    @GetMapping("/api/v1/brand/{id}")
    BrandVO getBrand(@PathVariable Long id);

    @Component
    class BrandFallBack implements  BrandClient{

        @Override
        public BrandVO getBrand(Long id) {
            BrandVO brandVO = new BrandVO();
            brandVO.setId(id);
            return brandVO;
        }
    }
}

