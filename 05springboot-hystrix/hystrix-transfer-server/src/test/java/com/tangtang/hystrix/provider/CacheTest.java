package com.tangtang.hystrix.provider;

import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;
import com.tangtang.hystrix.provider.command.BrandSemaphoreCommand;
import com.tangtang.hystrix.provider.command.BrandThreadCommand;
import org.junit.Assert;
import org.junit.Test;

public class CacheTest {
    @Test
    public void testWithCacheHits() {
        HystrixRequestContext context = HystrixRequestContext.initializeContext();
        try {
            BrandThreadCommand command1 = new BrandThreadCommand(1L);
            BrandThreadCommand command2 = new BrandThreadCommand(2L);

            command1.observe().toBlocking().toFuture().get();
            command2.observe().toBlocking().toFuture().get();
            boolean responseFromCache = command1.isResponseFromCache();
            Assert.assertFalse(responseFromCache);

            //测试是否查询同样的id会请求缓存
            BrandThreadCommand command3 = new BrandThreadCommand(1L);
            command3.observe().toBlocking().toFuture().get();
            responseFromCache = command3.isResponseFromCache();
            Assert.assertTrue(responseFromCache);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            context.shutdown();
        }

    }

}
