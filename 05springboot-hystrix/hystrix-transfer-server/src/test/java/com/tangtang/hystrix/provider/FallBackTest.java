package com.tangtang.hystrix.provider;

import com.alibaba.fastjson.JSONObject;
import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;
import com.tangtang.hystrix.provider.command.BrandSemaphoreCommand;
import com.tangtang.hystrix.provider.command.BrandThreadCommand;
import com.tangtang.hystrix.provider.command.TestFallBackCommand;
import com.tangtang.hystrix.provider.vo.BrandVO;
import com.tangtang.hystrix.provider.vo.ProductVO;
import org.junit.Assert;
import org.junit.Test;

public class FallBackTest {
    @Test
    public void testWithCacheHits() {
        HystrixRequestContext context = HystrixRequestContext.initializeContext();
        try {
            TestFallBackCommand command1 = new TestFallBackCommand(1L);

            ProductVO execute = command1.execute();
            System.out.println(JSONObject.toJSONString(execute));
            //判断是否读取降级中的默认数据
            Assert.assertEquals(execute.getId().longValue(),99L);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            context.shutdown();
        }


    }

}
