

# hystrix-provider-server

包含所有接口的提供

# hystrix-transfer-server

command包下是原生hystrix对接口的调用

而client则是集成了SpringCloud之后hystrix的操作，主要就是降级的功能体现，这里面有两个小坑，

1. 就是当对client接口降级的时候一定要加上@Component,否则会异常
2. 我一开始偷懒直接把provider服务中的RequestMapping内容直接复制了一下，后来代码启动直接报错，因为有两个相同的RequestMapping配置

工程启动之后，可以简单的尝试一下，原生的接口调用方式

```
localhost:8088/api/v1/order/1
```

以及springcloud整合后的降级方式

```
localhost:8088/api/v1/brand/1 
上面id -1 -2会异常会直接降级
```

# hystrix-dashboard

这里面就是hystrix监控的服务了

工程运行起来之后，访问localhost:2002/hystrx

之后将transfer-server服务的地址输入

http://localhost:8088/hystrix.stream

# hystrix-turbin

因为不依赖turbin的话，上面的监控其实是单机的监控，只是能监控8088这一个端口的，考虑到这个项目是微服务项目，所以同一个服务会有多个节点，因此，引入了turbin

同样在dashboard启动之后，界面中输入

http://localhost:8089/turbine.stream

既可以对集群进行监控了