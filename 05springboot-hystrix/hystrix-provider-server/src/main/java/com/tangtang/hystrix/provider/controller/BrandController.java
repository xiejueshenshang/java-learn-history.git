package com.tangtang.hystrix.provider.controller;

import com.tangtang.hystrix.provider.vo.BrandVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/brand")
public class BrandController {
    Map<Long, String> map = new HashMap<>();
    {
        map.put(1L, "华为");
        map.put(2L, "iPhone");
        map.put(3L, "小米");
        map.put(4L, "诺基亚");
        map.put(5L, "锤子");
        map.put(6L, "中兴");
        map.put(7L, "糖果");
        map.put(8L, "索尼");
    }
    @GetMapping("{id}")
    public BrandVO getBrand(@PathVariable Long id){
        if(id ==-1L){
            throw new RuntimeException("服务器异常");
        }
        if(id ==-2L){
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        String name = map.get(id);
        if(StringUtils.isBlank(name)){
            name = "手机";
        }
        return new BrandVO(id, name);
    }
}
