package com.tangtang.hystrix.provider.controller;

import com.tangtang.hystrix.provider.vo.ProductVO;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    Map<Long, ProductVO> map = new HashMap<>();
    {
        map.put(1L, new ProductVO(1L, "华为 mate20 pro", new BigDecimal(8999), "中国",1L));
        map.put(2L, new ProductVO(2L, "iPhone 12", new BigDecimal(5999), "美国",2L));
        map.put(3L, new ProductVO(3L, "小米 10", new BigDecimal(4999), "中国",3L));
        map.put(4L, new ProductVO(4L, "诺基亚 9090", new BigDecimal(1299), "中国",4L));
        map.put(5L, new ProductVO(5L, "锤子 Z01", new BigDecimal(3988), "中国",5L));
        map.put(6L, new ProductVO(1L, "中兴手机", new BigDecimal(4000), "中国",6L));
        map.put(7L, new ProductVO(1L, "糖果手机", new BigDecimal(1888), "中国",7L));
        map.put(8L, new ProductVO(1L, "索尼耳机", new BigDecimal(1999), "日本",8L));
    }
    @GetMapping("{id}")
    public ProductVO getProduct(@PathVariable Long id){
        ProductVO productVO = map.get(id);
        if(productVO == null){
            productVO = new ProductVO(id, "老年机", new BigDecimal(299), "中国", 9L);
        }
        return productVO;
    }
}
