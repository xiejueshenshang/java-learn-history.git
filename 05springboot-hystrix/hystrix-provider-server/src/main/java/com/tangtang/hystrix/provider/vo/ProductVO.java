package com.tangtang.hystrix.provider.vo;

import java.math.BigDecimal;


public class ProductVO {
    private Long id;
    private String name;
    private BigDecimal price;
    private String address;
    private Long brandId;

    public ProductVO() {
    }

    public ProductVO(Long id, String name, BigDecimal price, String address, Long brandId) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.address = address;
        this.brandId = brandId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
