package com.tangtang.register.server;

import lombok.Data;

@Data
public class RegisterRequest {
    private String serverName;

    private String ip;

    private String hostName;

    private int port;

    private String serviceInstenceId;


}
