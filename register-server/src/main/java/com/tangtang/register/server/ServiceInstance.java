package com.tangtang.register.server;

import lombok.Data;

@Data
public class ServiceInstance {
    private String serverName;

    private String ip;

    private String hostName;

    private int port;

    private String serviceInstenceId;

    private Lease lease;

    @Data
    public static class Lease{
        private Long latestHearbeatTime = System.currentTimeMillis();


        public void renew() {
            this.latestHearbeatTime =  System.currentTimeMillis();
        }
    }
}
