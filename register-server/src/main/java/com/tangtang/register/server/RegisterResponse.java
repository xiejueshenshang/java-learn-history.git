package com.tangtang.register.server;

import lombok.Data;

@Data
public class RegisterResponse {

    public final static String SUCCESS = "SUCCESS";
    public final static String FAILURE = "FAILURE";

    private String status;

    private Integer code;


}
