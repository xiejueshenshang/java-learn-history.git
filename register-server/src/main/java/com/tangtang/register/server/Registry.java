package com.tangtang.register.server;

import java.util.HashMap;
import java.util.Map;

public class Registry {
    private volatile static Registry registry;

    private Registry() {
    }


    private Map<String, Map<String, ServiceInstance>> map =
            new HashMap<>();

    public static Registry getInstance() {
        if (registry == null) {
            synchronized (Registry.class) {
                if (registry == null) {
                    registry = new Registry();
                }
            }
        }
        return registry;
    }

    public void register(ServiceInstance instance) {
        Map<String, ServiceInstance> serviceInstanceMap = this.map.get(instance.getServerName());
        if (serviceInstanceMap == null) {
            serviceInstanceMap = new HashMap<>();
            map.put(instance.getServerName(), serviceInstanceMap);
        }
        serviceInstanceMap.put(instance.getServiceInstenceId(), instance);
    }

    public  ServiceInstance getServiceInstance(String serviceName,String serviceInstanceId){
        return map.get(serviceName).get(serviceInstanceId);
    }

}
