package com.tangtang.project.spring.controller;

import com.tangtang.project.spring.domain.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.RestController;

import java.net.URL;
import java.util.Arrays;
import java.util.List;

@PropertySource("classpath:my.properties")
@RestController("valueController")
public class ValueController {
    /**
     * 获取位于application.properties中配置的属性
     */
    @Value("${user.name}")
    private String name;


    /**
     * 获取位于my.properties中的配置属性
     */
    @Value("${user.password}")
    private String password;

    //===================================================

    /**
     * 注入数组（自动根据","分割）
     */

    @Value("${tools}")

    private String[] toolArray;

    /**
     * 注入列表形式（自动根据","分割）
     */

    @Value("${tools}")
    private List<String> toolList;

    //===================================================

    @Value("测试")
    private String wechatSubscription;

    /**
     * 注入操作系统属性，例如操作系统的名字，代码如下：
     */
    @Value("#{systemProperties['os.name']}")

    private String systemPropertiesName;

    /**
     * 注入表达式结果，代码如下：
     */
    @Value("#{ T(java.lang.Math).random() * 100.0 }")
    private double randomNumber;

    //===================================================




    /**
     * 注入其他Bean属性：注入config对象的属性tool，代码如下：
     */
    @Value("#{config.tool}")
    private String tool;

    /**
     * 注入列表形式（自动根据"|"分割），代码如下：
     */
    @Value("#{'${words}'.split('\\|')}")
    private List<String> numList;

    /**
     * 注入文件资源，代码如下：
     */
    @Value("classpath:beans.xml")
    private Resource resourceFile;

    /**
     * 注入URL资源，代码如下：
     */

    @Value("http://www.choupangxia.com")
    private URL homePage;

    //===================================================

    @Value("${ip:127.0.0.1}")
    private String ip;

    /**
     * 如果系统属性中未获取到port的值，则使用8888，代码如下：
     */
    @Value("#{systemProperties['port']?:'8888'}")
    private String port;


    @Override
    public String toString() {
        return "ValueController{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", toolArray=" + Arrays.toString(toolArray) +
                ", toolList=" + toolList +
                ", wechatSubscription='" + wechatSubscription + '\'' +
                ", systemPropertiesName='" + systemPropertiesName + '\'' +
                ", randomNumber=" + randomNumber +
                ", tool='" + tool + '\'' +
                ", numList=" + numList +
//                ", resourceFile=" + resourceFile +
                ", homePage=" + homePage +
                ", ip='" + ip + '\'' +
                ", port='" + port + '\'' +
                '}';
    }
}
