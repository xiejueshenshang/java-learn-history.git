package com.tangtang.project.spring.aop.proxy.jdk;

import org.springframework.cglib.proxy.InvocationHandler;

import java.lang.reflect.Method;

public class DynamicProxy implements InvocationHandler {
    //代理的真实对象
    private Object service;

    //构造方法， 给代理的真实对象赋值
    public  DynamicProxy(Object service){
        this.service = service;
    }

    @Override
    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
        //真实方法之前执行
        System.out.println("before");

        //调用真实方法
        method.invoke(service, objects);

        //真实方法之后执行
        System.out.println("after");
        return null;
    }
}
