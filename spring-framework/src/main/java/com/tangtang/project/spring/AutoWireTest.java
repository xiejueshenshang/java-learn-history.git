package com.tangtang.project.spring;

import com.tangtang.project.spring.domain.Personal;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.annotation.Resource;

public class AutoWireTest {

    private   TestUtil testUtil;


    @Resource
    public void setTestUtil(TestUtil testUtil) {
        this.testUtil = testUtil;
    }

    public static void main(String[] args) {
        BeanFactory beanFactory = new ClassPathXmlApplicationContext("/autowire.xml");
        TestUtil testNameUtil = (TestUtil) beanFactory.getBean("testNameUtil");
        Personal personel = testNameUtil.getPersonal();
        System.out.println(personel);


        TestUtil testTypeUtil = (TestUtil) beanFactory.getBean("testTypeUtil");
        Personal personel1 = testTypeUtil.getPersonal();
        System.out.println(personel1);



        AutoWireTest autoWireTest = new AutoWireTest();
        System.out.println(autoWireTest.getPersonal());

    }


    public  Personal getPersonal(){
       return testUtil.getPersonal();
    }


}
