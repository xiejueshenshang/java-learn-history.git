package com.tangtang.project.spring.loopdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Dog {
//    @Autowired
    public Animal animal = new Animal(null);

    public Dog(Animal animal) {
        this.animal = animal;
    }
}
