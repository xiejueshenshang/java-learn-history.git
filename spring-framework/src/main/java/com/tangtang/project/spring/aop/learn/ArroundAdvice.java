package com.tangtang.project.spring.aop.learn;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;


public class ArroundAdvice implements MethodInterceptor {


    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        System.out.println("方法执行之前增强");
        Object ret = methodInvocation.proceed();
        System.out.println("方法执行之后增强");
        return ret;
    }
}
