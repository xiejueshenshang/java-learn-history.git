package com.tangtang.project.spring.domain;


import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class Personal {
    private int id;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Personel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
