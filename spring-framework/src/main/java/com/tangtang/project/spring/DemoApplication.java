package com.tangtang.project.spring;

import com.tangtang.project.spring.domain.Config;
import com.tangtang.project.spring.domain.Rumenz;
import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class DemoApplication {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = null;
        try {
            applicationContext = new AnnotationConfigApplicationContext();
            applicationContext.register(DemoApplication.class);
            applicationContext.refresh();
            //NoSuchBeanDefinitionException：No qualifying bean of type 'com.ruyuan.little.project.spring.domain.Rumenz' available
            byBeanFactory(applicationContext);
            applicationContext.close();
        } catch (BeansException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }


        try {
            applicationContext = new AnnotationConfigApplicationContext();
            applicationContext.register(Config.class);
            applicationContext.refresh();
            //NoUniqueBeanDefinitionException：No qualifying bean of type 'com.ruyuan.little.project.spring.domain.Rumenz' available: expected single matching bean but found 2: rumenz1,rumenz2
            byBeanFactory(applicationContext);
            applicationContext.close();
        } catch (BeansException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    private static void byBeanFactory(AnnotationConfigApplicationContext applicationContext){
        applicationContext.getBean(Rumenz.class);
    }


}
