package com.tangtang.project.spring.aop.proxy.jdk;

import org.springframework.cglib.proxy.InvocationHandler;
import org.springframework.cglib.proxy.Proxy;


public class Client {
    public static void main(String[] args) {
        //要代理的真实对象
        Service service = new ServiceImpl();
        //要代理的哪个真实对象， 就该将对象传进去，最后通过该怎是对象来调用其方法
        InvocationHandler handler = new DynamicProxy(service);
        //添加以下的几段代码，就可以将代理生成的字节码存储起来
        try {
            Service serviceProxy = (Service) Proxy.newProxyInstance(service.getClass().getClassLoader(),
                    service.getClass().getInterfaces(), handler);
            serviceProxy.help();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
