package com.tangtang.project.spring.aop.learn;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class TestAop {
    public static void main(String[] args) {
        ApplicationContext context = new GenericXmlApplicationContext("/aop.xml");
        AopBiz biz =  context.getBean(AopBiz.class);
        biz.help();
        biz.service();
    }
}
