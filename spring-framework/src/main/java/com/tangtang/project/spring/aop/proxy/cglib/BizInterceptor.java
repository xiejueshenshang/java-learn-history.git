package com.tangtang.project.spring.aop.proxy.cglib;

import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class BizInterceptor implements MethodInterceptor {
    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        //真实方法之前执行
        System.out.println("before");

        //调用真实方法
        methodProxy.invokeSuper(o, objects);

        //真实方法之后执行
        System.out.println("after");
        return null;
    }
}
