package com.tangtang.project.spring;

import com.tangtang.project.spring.domain.Personal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class TestUtil {
    private Personal personal;

    public Personal getPersonal() {
        return personal;
    }
    @Resource
    public void setPersonal(Personal personal) {
        this.personal = personal;
    }
}
