package com.tangtang.project.spring.loopdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.stereotype.Component;

@Component
public class Animal {
    @Autowired
    public Mammals mammals;

    public Animal(Mammals mammals) {
        this.mammals = mammals;
    }
}
