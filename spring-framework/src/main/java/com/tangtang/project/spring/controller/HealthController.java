package com.tangtang.project.spring.controller;

import com.ruyuan.little.project.common.dto.CommonResponse;
import com.tangtang.project.spring.loopdemo.Animal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:健康检查的controller
 **/
@RestController
public class HealthController {

    @Autowired
    private  ValueController valueController;

    @Autowired
    private Animal animal;

    @RequestMapping(value = "/")
    public CommonResponse health() {
        System.out.println(valueController);
        return CommonResponse.success();
    }

    @RequestMapping(value = "/loop")
    public void loop(){
        System.out.println(animal);
    }

}