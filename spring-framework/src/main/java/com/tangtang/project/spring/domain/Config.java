package com.tangtang.project.spring.domain;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component("config")
public class Config {
    @Value("cofig.tool")
    public String tool;


    @Bean
    public Rumenz rumenz1(){
        Rumenz rumenz = new Rumenz();
        rumenz.setId(1);
        rumenz.setName("张思睿");
        return rumenz;
    }

    @Bean
    public Rumenz rumenz2(){
        Rumenz rumenz = new Rumenz();
        rumenz.setId(2);
        rumenz.setName("李思");
        return rumenz;
    }
}
