package com.tangtang.project.spring.loopdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Mammals {
    @Autowired
    public Dog dog;

    public Mammals(Dog dog) {
        this.dog = dog;
    }
}
