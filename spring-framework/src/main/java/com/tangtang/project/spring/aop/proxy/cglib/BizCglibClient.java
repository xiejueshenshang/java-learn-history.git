package com.tangtang.project.spring.aop.proxy.cglib;

import org.springframework.cglib.proxy.Enhancer;

public class BizCglibClient {
    public static void main(String[] args) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(Biz.class);
        enhancer.setCallback(new BizInterceptor());
        Biz biz = (Biz) enhancer.create();
        biz.help();
    }
}
