package com.tangtang.feign.provider.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

/**
 * feign文件上传的练习
 */
@RestController
@RequestMapping("api/v1/file")
public class FileController {


    @PostMapping(name = "upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    String uploadFile(@RequestPart("file")  MultipartFile file) {
        if(file!=null){
            return "获取到上传文件的名称" + file.getOriginalFilename();
        }
        else{
            return "文件上传文件为空";
        }
    }

    //方式二 改为base64流进行文件上传
}
