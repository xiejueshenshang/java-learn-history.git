package com.tangtang.feign.provider.controller;

import com.tangtang.common.client.UserInterface;
import com.tangtang.common.client.vo.User;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

//RestController 这个哪怕是集成接口也要写，不写的话，是无法作为controller生效的
@RestController
public class UserControllerBk implements UserInterface {

    @Override
    public String hello(String name) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "hello "+name;
    }

    @Override
    public String createUser(@RequestBody User user) {
        System.out.println("创建用户，" + user);
        return "{'msg': 'success'}";
    }
    @Override
    public String updateUser(@PathVariable("id") Long id, @RequestBody User user) {
        System.out.println("更新用户，" + user);
        return "{'msg': 'success'}";
    }
    @Override
    public String deleteUser(@PathVariable("id") Long id) {
        System.out.println("删除用户，id=" + id);
        return "{'msg': 'success'}";
    }
    @Override
    public User getById(@PathVariable("id") Long id) {
        System.out.println("查询用户，id=" + id);
        return new User(id, "张三", 20);
    }
}
