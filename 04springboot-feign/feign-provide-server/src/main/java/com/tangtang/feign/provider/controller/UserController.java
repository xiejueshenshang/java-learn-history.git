package com.tangtang.feign.provider.controller;//package com.tangtang.feign.provider.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/userbk")
public class UserController {

    @GetMapping("hello/{name}")
    public String hello(@PathVariable("name") String name){
        System.out.println("方法被调用了");
        return "hello " + name;
    }
}
