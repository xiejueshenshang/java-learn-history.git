package com.tangtang.feign.transfer.client;

import com.tangtang.common.client.UserInterface;
import com.tangtang.feign.transfer.config.MyConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "feign-provide",configuration = MyConfiguration.class,contextId = "user")
//@FeignClient(name = "feign-provide",url = "http://127.0.0.1:8080",configuration = MyConfiguration.class)
//@RequestMapping("/api/v1/user")
public interface ProviderServerUserClient  extends UserInterface {
//        @GetMapping("hello/{name}")
//        public String hello(@PathVariable("name") String name);
}
