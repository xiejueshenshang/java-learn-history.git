package com.tangtang.feign.transfer.controller;

import com.tangtang.feign.transfer.client.FileClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * feign文件上传的练习
 */
//@RequestMapping("api/v2/file") 配置的值不能与feign配置的地址一样， 一般情况下也不会一样，所以这里改成了v2
@RestController
@RequestMapping("api/v2/file")
public class FileController {

    @Autowired
    private  FileClient fileClient;

    @PostMapping("upload")
    String uploadFile(MultipartFile file) {
        return fileClient.uploadFile(file);
    }
}
