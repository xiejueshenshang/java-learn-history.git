package com.tangtang.feign.transfer.client;

import com.tangtang.common.client.UserInterface;
import com.tangtang.feign.transfer.config.MyConfiguration;
import feign.codec.Encoder;
import feign.form.spring.SpringFormEncoder;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@FeignClient(name = "feign-provide",contextId = "file")
@RequestMapping("api/v1/file")
public interface FileClient {
    @PostMapping(name = "upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    String uploadFile( @RequestPart("file") MultipartFile file);
}
