package com.tangtang.feign.transfer.controller;//package com.tangtang.feign.provider.controller;


import com.tangtang.common.client.vo.User;
import com.tangtang.feign.transfer.client.ProviderServerUserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v2/user")
public class UserController {
    @Autowired
    ProviderServerUserClient userClient;

    @GetMapping("hello/{name}")
    public String hello(@PathVariable("name") String name) {
        return userClient.hello(name);
    }


    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String createUser(@RequestBody User user) {
        return userClient.createUser(user);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public String updateUser(@PathVariable("id") Long id, @RequestBody User user) {
        return userClient.updateUser(id, user);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public String deleteUser(@PathVariable("id") Long id) {
        return userClient.deleteUser(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public User getById(@PathVariable("id") Long id) {
        return userClient.getById(id);
    }

}
