package com.tangtang.feign.transfer.config;

import feign.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class MyConfiguration {
    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    @Bean
    Retryer retryer(){
        return new Retryer.Default.Default();
    }

    //配置是在FeignClient中的接口中使用Feign自带的注解
//    @Bean
//    public Contract feignContract(){
//        return new feign.Contract.Default();
//    }

    //禁用Hystrix
//    @Bean
//    @Scope("prototype")
//    public Feign.Builder feignBuilder() {
//        return Feign.builder();
//    }

    //产生报错Method ProviderServerUserClient#hello(String) not annotated with HTTP method type (ex. GET, POST) 需要使用RequestMapping来代替GetMapping或者PostMapping
    //设置拦截器
//    @Bean
//    public RequestInterceptor requestInterceptor() {
//        return new MyRequestInterceptor();
//    }

}
