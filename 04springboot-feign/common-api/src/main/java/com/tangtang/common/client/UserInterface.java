package com.tangtang.common.client;

import com.tangtang.common.client.vo.User;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user")
public interface UserInterface {
    @GetMapping("hello/{name}")
    public String hello(@PathVariable("name") String name);

    @PostMapping(value = "/")
    String createUser(@RequestBody User user);

    @PutMapping(value = "/{id}")
    String updateUser(@PathVariable("id") Long id, @RequestBody User user);

    @DeleteMapping(value = "/{id}")
    String deleteUser(@PathVariable("id") Long id);

    @GetMapping(value = "/{id}")
    User getById(@PathVariable("id") Long id);
}
