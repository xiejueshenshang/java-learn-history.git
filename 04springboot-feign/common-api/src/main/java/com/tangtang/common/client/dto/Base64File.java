package com.tangtang.common.client.dto;


import java.io.Serializable;

public class Base64File implements Serializable {
    /**
     * 文件名称
     */
    String fileName;

    /**
     * 返回的base64流
     */
    String base64;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }
}
