package com.tangtang.transactional.domain;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

@Data
public class Order {
    private Long id;

    private String orderNo;

    private BigDecimal totalAmount = new BigDecimal(0);

    private BigDecimal taxAmount = new BigDecimal(0);

    private Integer status;

    private Integer isDeleted;

    private Date createdTime;

    private Date modifiedTime;

}
