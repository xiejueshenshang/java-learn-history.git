package com.tangtang.transactional.domain;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class OrderLine {
    private Long id;

    private Long orderId;

    private String productName;

    private BigDecimal price;

    private BigDecimal tax;

    private BigDecimal amount;

    private BigDecimal quantity;

    private Integer isDeleted;

    private Date createdTime;

    private Date modifiedTime;

}
