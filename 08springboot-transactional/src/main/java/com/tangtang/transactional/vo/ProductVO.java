package com.tangtang.transactional.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductVO {
    private String productName;

    private BigDecimal quantity;

    private BigDecimal price;

    private BigDecimal tax;
}
