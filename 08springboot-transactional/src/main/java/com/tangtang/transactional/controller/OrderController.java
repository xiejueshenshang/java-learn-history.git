package com.tangtang.transactional.controller;

import com.tangtang.transactional.service.OrderLineService;
import com.tangtang.transactional.service.OrderService;
import com.tangtang.transactional.vo.ProductVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderLineService orderLineService;

    /*
    [
      {
        "price": 9999,
        "productName": "iPhone12 Pro MAX",
        "quantity": 2,
        "tax":1299
      },
    {
        "price": 5999,
        "productName": "iPhone12",
        "quantity": 3,
        "tax": 780
      }
    ]
    */
    @PostMapping("addOrder")
    public Boolean addOrder(@RequestBody List<ProductVO> products) {
        return orderService.addOrder(products);
    }
}
