package com.tangtang.transactional.mapper;

import com.tangtang.transactional.domain.OrderLine;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderLineMapper {

    @Insert("insert into order_line(id,order_id,product_name,price,tax,amount,quantity) " +
            "values(" +
            "#{id}," +
            "#{orderId}," +
            "#{productName}," +
            "#{price}," +
            "#{tax}," +
            "#{amount}," +
            "#{quantity}" +
            ") ")
    boolean insert(OrderLine orderLine);
}
