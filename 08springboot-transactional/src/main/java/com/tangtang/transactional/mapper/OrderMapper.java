package com.tangtang.transactional.mapper;

import com.tangtang.transactional.domain.Order;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderMapper {

    @Insert("insert into goods_order (id,order_no,total_amount,tax_amount) " +
            "values(" +
            "#{id}," +
            "#{orderNo}," +
            "#{totalAmount}," +
            "#{taxAmount}" +
            ") ")
    boolean insert(Order order);
}
