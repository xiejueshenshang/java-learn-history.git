package com.tangtang.transactional.service;

import com.tangtang.transactional.domain.OrderLine;
import org.springframework.stereotype.Service;

@Service
public interface OrderLineService {
    boolean insert(OrderLine orderLine);
}
