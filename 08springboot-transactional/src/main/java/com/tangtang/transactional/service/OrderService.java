package com.tangtang.transactional.service;

import com.tangtang.transactional.domain.Order;
import com.tangtang.transactional.vo.ProductVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public interface OrderService {
    boolean insert(Order order);


    @Transactional(rollbackFor = Exception.class)
    boolean addOrder(@RequestBody List<ProductVO> products);
}
