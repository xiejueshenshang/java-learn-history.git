package com.tangtang.transactional.service.impl;

import com.tangtang.transactional.domain.OrderLine;
import com.tangtang.transactional.mapper.OrderLineMapper;
import com.tangtang.transactional.service.OrderLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OrderLineServiceImpl implements OrderLineService {

    @Autowired
    private OrderLineMapper orderLineMapper;

    /**
     * Propagation.REQUIRED 默认的，如果调用的开启事务，则使用调用方的事务，否则开启一个新事务
     * Propagation.REQUIRES_NEW  开启一个新的事务，不使用调用方的事务直接开启一个新事务
     * Propagation.SUPPORTS 使用当前已经开启的事务，如果没有事务，则以没有事务的方式进行运行
     * Propagation.MANDATORY 支持当前事务，必须有一个开启的事务 ，否则报异常
     * Propagation.NOT_SUPPORTED 非事务方式执行操作，如果当前存在事务，就把当前事务挂起。
     * Propagation.NEVER 以非事务方式执行，如果当前存在事务，则抛出异常。
     * Propagation.NESTED 如果当前存在事务，则在嵌套事务内执行。如果当前没有事务，则进行与PROPAGATION_REQUIRED类似的操作。
     *
     * @param orderLine
     * @return
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.NEVER)
    @Override
    public boolean insert(OrderLine orderLine) {
        return orderLineMapper.insert(orderLine);
    }

}
