package com.tangtang.designpatterns.singleton.idgenerator;

import java.util.concurrent.atomic.AtomicLong;

/**
 * 静态内部类
 */
public class IdGeneratorStatic {
  private AtomicLong id = new AtomicLong(0);
  private IdGeneratorStatic() {}
  private static class SingletonHolder{
    private static final IdGeneratorStatic instance = new IdGeneratorStatic();
  }
  
  public static IdGeneratorStatic getInstance() {
    return SingletonHolder.instance;
  }
 
  public long getId() { 
    return id.incrementAndGet();
  }
}
