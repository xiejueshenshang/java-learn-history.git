package com.tangtang.designpatterns.singleton.idgenerator;

import java.util.concurrent.atomic.AtomicLong;

/**
 * 饿汉式
 */
public class IdGeneratorHunger {
  private AtomicLong id = new AtomicLong(0);
  private static final IdGeneratorHunger instance = new IdGeneratorHunger();
  private IdGeneratorHunger() {}
  public static IdGeneratorHunger getInstance() {
    return instance;
  }
  public long getId() { 
    return id.incrementAndGet();
  }
}