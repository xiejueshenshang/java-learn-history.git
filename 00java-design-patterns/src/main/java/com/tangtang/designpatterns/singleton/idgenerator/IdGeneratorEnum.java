package com.tangtang.designpatterns.singleton.idgenerator;

import java.util.concurrent.atomic.AtomicLong;

/**
 * 枚举
 */
public enum IdGeneratorEnum {
  INSTANCE;
  private AtomicLong id = new AtomicLong(0);
 
  public long getId() { 
    return id.incrementAndGet();
  }
}