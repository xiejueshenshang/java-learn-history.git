package com.tangtang.designpatterns.factory.config;

public interface IRuleConfigParser {
    RuleConfig parse(String data);
}
