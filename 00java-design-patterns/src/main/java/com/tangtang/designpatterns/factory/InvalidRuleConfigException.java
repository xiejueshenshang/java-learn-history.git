package com.tangtang.designpatterns.factory;

public class InvalidRuleConfigException extends Throwable {
    public InvalidRuleConfigException(String message) {
        super(message);
    }
}
