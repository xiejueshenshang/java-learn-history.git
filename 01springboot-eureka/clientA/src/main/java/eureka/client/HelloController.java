package eureka.client;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class HelloController {

    @GetMapping("hello/{name}")
    public Map<String,String> hello(@PathVariable String name){
        Map<String,String> result = new HashMap<>();
        result.put("msg", "hello" + name);
        result.put("fromServer", "clientA");
        return result;
    }

}
