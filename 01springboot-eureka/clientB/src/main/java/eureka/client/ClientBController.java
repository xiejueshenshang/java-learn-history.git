package eureka.client;

import com.alibaba.fastjson.JSONObject;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@RestController
@Configuration
public class ClientBController {

    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @RequestMapping(value = "/greeting/{name}", method = RequestMethod.GET)
    public Map<String, String> greeting(@PathVariable("name") String name) {
        RestTemplate restTemplate = getRestTemplate();
        Map<String,String> mesage = restTemplate.getForObject("http://clientA/hello/" + name, Map.class);
        mesage.put("nowServer", "clientB");
        return mesage;
    }

}
