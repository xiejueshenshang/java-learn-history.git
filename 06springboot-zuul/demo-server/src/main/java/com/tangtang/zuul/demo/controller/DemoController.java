package com.tangtang.zuul.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("user")
@RestController
public class DemoController {
    @GetMapping("hello/{name}")
    public String hello(@PathVariable(name = "name") String name){
        return "hello " + name;
    }
}
