package com.tangtang.basic.demo;

import cn.hutool.core.util.HexUtil;
import cn.hutool.crypto.SecureUtil;

public class Demo {
    public static void main(String[] args) {
//        HexUtil.encodeHexStr()
        String str = "HELLO WORLD";
        String md5Str = SecureUtil.md5(str).substring(8, 24);
        String hexStr = HexUtil.encodeHexStr(md5Str);
        System.out.println(hexStr);
        System.out.println(hexStr.length());
        String decodeMd5 = new String(HexUtil.decodeHex(hexStr));
        System.out.println(decodeMd5);
        System.out.println(md5Str.equals(decodeMd5));
    }
}
