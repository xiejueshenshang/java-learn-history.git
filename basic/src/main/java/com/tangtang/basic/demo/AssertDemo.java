package com.tangtang.basic.demo;

import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

public class AssertDemo {
    static   Map<Long, Long>  hasSovledList = new HashMap<>();


    public static void main(String[] args) {
        Long result = f(100L);
        System.out.println(result);
    }

    public static  Long f(Long n) {
        if (n == 1) {
            return 1L;
        }
        if (n == 2) {
            return 2L;
        }

        // hasSolvedList 可以理解成一个 Map，key 是 n，value 是 f(n)
        if (hasSovledList.containsKey(n)) {
            return hasSovledList.get(n);
        }

        Long ret = f(n-1) + f(n-2);
        hasSovledList.put(n, ret);
        return ret;
    }
}
