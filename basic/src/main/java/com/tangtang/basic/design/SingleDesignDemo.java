package com.tangtang.basic.design;

public class SingleDesignDemo {
    private volatile  static SingleDesignDemo instance = null;
    public static SingleDesignDemo getInstance(){
        if (instance == null) {
            synchronized (SingleDesignDemo.class){
                if (instance == null) {
                    SingleDesignDemo.instance = new SingleDesignDemo();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return instance;
    }


    public static void main(String[] args) {
        ThreadGroup threadGroup = new ThreadGroup("SingleDesignGroup");
        for (int i = 0; i < 100; i++) {
            Thread thread = new Thread(threadGroup,"thread"+i){
                @Override
                public void run() {
                    SingleDesignDemo singleDesignDemo = SingleDesignDemo.getInstance();
                    System.out.println(singleDesignDemo.toString());
                }
            };
            thread.start();

        }


    }
}
