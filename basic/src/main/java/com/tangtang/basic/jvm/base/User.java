package com.tangtang.basic.jvm.base;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class User {

    private String name;
    private int age;
    private Hobby hobby;


}

