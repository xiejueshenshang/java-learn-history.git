package com.tangtang.basic.jvm.base;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class WeakReferenceTest { volatile static List<User> list = new ArrayList<>();
    private static  Hobby hobby = new Hobby();
    static WeakReference<List> listList = new WeakReference<List>(new ArrayList());
    public static void main(String[] args) {

        hobby.setHobby("Game and Study");

        new Thread(  new Runnable() {
            @Override
            public void run() {
                while (true) {

                    User user = new User();
                    user.setAge(1);
                    user.setName("huhansan");
                    user.setHobby(hobby);
                    list.add(user);
                    if (listList.get() == null){
                        listList = new WeakReference<List>(new ArrayList());
                    }
                    if(list.size()>1000){
                        listList.get().add(list);
//                        listList.add(list);
                        list = new ArrayList<>();
                    }
//                    try {
//                        Thread.sleep(1);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }

                }
            }
        }).start();
        new Thread(  new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if(listList.get()!=null) {
                        System.out.println(listList.get().size());
                    }
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
