package com.tangtang.basic.jvm.base;


import java.util.ArrayList;
import java.util.List;

/**
 * JVM配置如下：
 *     -Xms2m -Xmx2m -Xmn1m
 */
public class SimpleTest {

    private static  Hobby hobby = new Hobby();
    volatile static List<User>  list = new ArrayList<>();
    public static void main(String[] args) {
        List<List<User>> listList = new ArrayList<>();
        hobby.setHobby("Game and Study");

        doSome();

        new Thread(  new Runnable() {
            @Override
            public void run() {
                while (true) {

                    User user = new User();
                    user.setAge(1);
                    user.setName("huhansan");
                    user.setHobby(hobby);
                    list.add(user);
                    if(list.size()>1000){
                        listList.add(list);
                        list = new ArrayList<>();
                    }
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
        new Thread(  new Runnable() {
            @Override
            public void run() {
                while (true) {
                    System.out.println(listList.size());
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public static void doSome(){
        Other user = new Other();
    }
}
