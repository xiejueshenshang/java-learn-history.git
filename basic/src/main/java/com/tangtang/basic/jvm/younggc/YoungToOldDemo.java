package com.tangtang.basic.jvm.younggc;

//-XX:NewSize=10m
//-XX:MaxNewSize=10m
//-XX:InitialHeapSize=20m
//-XX:MaxHeapSize=20m
//-XX:SurvivorRatio=6
//-XX:MaxTenuringThreshold=15
//-XX:PretenureSizeThreshold=10m
//-XX:+UseParNewGC
//-XX:+UseConcMarkSweepGC
//-XX:+PrintGCDetails
//-XX:+PrintGCTimeStamps
//-Xloggc:gc.log
public class YoungToOldDemo {
    public static  void main(String[] args){
        byte[] array1 = new byte[2 * 1024 * 1024];
        array1 = new byte[2 * 1024 * 1024];
        array1 = new byte[2 * 1024 * 1024];
        array1 = null;

        //定义一个不会被释放的128kb的array2
        byte[] array2 = new byte[128* 1024];

        for(int i = 0;i<9999;i++){
//        while (true){
            byte[] array = new byte[2* 1024 * 1024];
            array = new byte[2* 1024 * 1024];
            array = new byte[2* 1024 * 1024];
            array = null;

        }


        try {
            Thread.sleep(1000000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

//        byte[] array2 = new byte[10* 1024 * 1024];

    }
}