package com.tangtang.basic.jvm.base;

import lombok.Data;

@Data
public class Hobby {

    private String hobby;
}
