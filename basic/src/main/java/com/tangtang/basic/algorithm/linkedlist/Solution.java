package com.tangtang.basic.algorithm.linkedlist;


import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Solution {
    public ListNode reverseList(ListNode head) {
        if(head == null || head.next ==  null){
            return head;
        }
        ListNode newNode = null;
        ListNode sonNode = null;
        while(head != null){
            newNode = new ListNode();
            newNode.val = head.val;
            newNode.next = sonNode;
            sonNode = newNode;
            head = head.next;
        }
        return newNode;
    }



    /**
     * 链表中环的检测
     * @param head
     * @return 下面方式很low有点取消，还需要创建一个map 主要借用了取地址，判断是否存在
     */
    public boolean hasCycle1(ListNode head) {
        if(head == null || head.next ==  null){
            return false;
        }
        Map<Integer, ListNode> map = new HashMap<>();
        while (head !=null){
            map.put(head.hashCode(), head);
            head = head.next;
            if(head == null){
                return false;
            }
            if(map.get(head.hashCode())!=null){
                return true;
            }
        }

        return false;
    }

    /**
     * 链表中环的检测
     * @param head
     * @return 快慢指针
     */
    public boolean hasCycle(ListNode head) {
        ListNode slow = head;
        ListNode fast = head;
        while(fast!=null&& fast.next !=null){
            fast = fast.next.next;
            slow = slow.next;
            if(fast == slow){
                return true;
            }
        }

        return false;
    }

    /**
     * 删除链表的倒数第 N 个结点
     * @param head
     * @param n
     * @return
     */
    public ListNode removeNthFromEnd(ListNode head, int n) {
        return null;
    }

    public static void main(String[] args) {
//        ListNode listNode = ListNode.generateListNode(1, 2, 3, 4, 5);
//        Solution solution = new Solution();
//        ListNode reverseList = solution.reverseList(listNode);

        ListNode listNode = ListNode.generateListNode(1, 2);
        listNode.next.next = listNode;

    }
}
