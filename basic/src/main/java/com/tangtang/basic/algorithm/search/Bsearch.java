package com.tangtang.basic.algorithm.search;

public class Bsearch {
    public int bsearch(int[] a,int value) {
        int low = 0;
        int high = a.length - 1;

        while (low <= high) {
            int mid = low+(high-low)/2;
            if (a[mid] == value) {
                return mid;
            } else if (a[mid] < value) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }

        return -1;
    }
}
