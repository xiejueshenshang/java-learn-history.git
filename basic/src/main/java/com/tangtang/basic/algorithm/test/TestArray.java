package com.tangtang.basic.algorithm.test;


import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

public class TestArray {
    public static void main(String[] args) {
        int count = 0;
        int average = 1;
        for (int i = 0; i <average ; i++) {
            count += getArray(100);
        }
        int i = count / average;
        System.out.println(i);


    }

    private static int getArray(int length) {
        int count = 0;
        long start = System.currentTimeMillis();

        int[] arrays = new int[length];
        Set<Integer> set = new LinkedHashSet<>();
        Random random = new Random();
        while (set.size() < length) {
            int i = random.nextInt(length);
            count++;
            set.add(i +1);
        }
        int index = 0;
        for (Integer num : set) {
            arrays[index++] = num;
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start+"ms");
        System.out.println(Arrays.toString(arrays));
        System.out.println(arrays.length);
        System.out.println("总共执行了："+count+"次random()函数");
        return count;
    }
}
