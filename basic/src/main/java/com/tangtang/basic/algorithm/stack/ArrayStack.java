package com.tangtang.basic.algorithm.stack;

public class ArrayStack {
    // 数组
    private String[] items;
    // 栈中元素个数
    private int count;
    // 栈的大小
    private int n;

    public ArrayStack(int n) {
        this.items = new String[n];
    }

    public void push(String value) {
        if (items.length == n) {
            //动态扩容
            String[] newStack =  new String[n * 2];
            for (int i = 0; i < items.length; i++) {
                newStack[i] = items[i];
            }
            items = newStack;
        }
        items[n] = value;
        n++;
    }

    public String pop() {
        if (n == 0) {
            return null;
        }
        String value = items[n - 1];
        n--;
        return value;
    }

    public static void main(String[] args) {
        ArrayStack arrayStack = new ArrayStack(3);
        arrayStack.push("A");
        arrayStack.push("B");
        arrayStack.push("C");
        arrayStack.push("1");
        arrayStack.push("2");
        arrayStack.push("3");
        System.out.println(arrayStack.pop());
        System.out.println(arrayStack.pop());
        System.out.println(arrayStack.pop());
        arrayStack.push("E");
        System.out.println(arrayStack.pop());
        System.out.println(arrayStack.pop());
        arrayStack.push("F");
        arrayStack.push("G");
        arrayStack.push("H");
        System.out.println(arrayStack.pop());
        System.out.println(arrayStack.pop());
        System.out.println(arrayStack.pop());
        System.out.println(arrayStack.pop());
        System.out.println(arrayStack.pop());
    }
}
