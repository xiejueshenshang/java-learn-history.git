package com.tangtang.basic.algorithm.stack;


public class LinkedStack {
    //存储的数据
    String item;
    //子节点
    LinkedStack nextNode;
    //父节点
    LinkedStack preNode;
    //最后一个节点
    LinkedStack lastNode;

    public LinkedStack(){
        nextNode = null;
        preNode = null;
        lastNode = null;
    }


    public void  push(String value){
        this.item = value;
        LinkedStack temp = null;
        if(lastNode!= null){
            //上一个节点
            temp = lastNode;
            temp.nextNode = this;
        }
        this.lastNode = new LinkedStack();
        this.lastNode.item = value;
        this.preNode = temp;
    }

    public String pop(){
        String value = this.lastNode.item;
        this.lastNode.preNode.nextNode = null;
        this.lastNode = this.lastNode.preNode;
        return value;
    }

    public static void main(String[] args) {
        LinkedStack linkedStack = new LinkedStack();
        linkedStack.push("A");
        linkedStack.push("B");
        linkedStack.push("C");
        linkedStack.push("D");
    }

}
