package com.tangtang.basic.algorithm.linkedlist;

public class ListNode {
    int val;
    ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }


    public static ListNode generateListNode(int... val) {
        ListNode newNode = null;
        ListNode sonNode = null;
        for (int i = val.length - 1; i >= 0; i--) {
            newNode = new ListNode();
            newNode.val = val[i];
            newNode.next = sonNode;
            sonNode = newNode;
        }
        return newNode;
    }


    public static ListNode generateCycleListNode(int pos,int... val) {
        ListNode newNode = null;
        ListNode sonNode = null;
        for (int i = val.length - 1; i >= 0; i--) {
            newNode = new ListNode();
            newNode.val = val[i];
            newNode.next = sonNode;
            sonNode = newNode;
        }
        return newNode;
    }

    public static void main(String[] args) {
        generateListNode(1, 2, 3);
    }
}
