package com.tangtang.basic.lambda;

public class LambdaDemo {
    public static void main(String[] args) {
//        System.out.println((""+System.currentTimeMillis()).length());
        Userinterface u = getUser("张三");
        u.sayHello();
    }

    public static Userinterface getUser(String userName){
        return  () ->{
            System.out.println("hello world" + userName);
        };
    }
}
