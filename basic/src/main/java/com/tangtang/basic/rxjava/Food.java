package com.tangtang.basic.rxjava;

import java.math.BigDecimal;

public class Food {
    private Integer id;
    private String name;
    private BigDecimal price;
    private int makeTime;
    public Food() {
    }

    public Food(Integer id, String name, BigDecimal price, int makeTime) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.makeTime = makeTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public int getMakeTime() {
        return makeTime;
    }

    public void setMakeTime(int makeTime) {
        this.makeTime = makeTime;
    }
}
