package com.tangtang.basic.rxjava;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.functions.Func1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Demo {
    static  Map<Integer, Food> foodMapCache = new HashMap<>();
    static {
        foodMapCache.put(1, new Food(1,"地三鲜",new BigDecimal(28),10));
        foodMapCache.put(2, new Food(2,"水煮鱼",new BigDecimal(38),20));
        foodMapCache.put(3, new Food(3,"锅包肉",new BigDecimal(48),25));
        foodMapCache.put(4, new Food(4,"糖醋里脊",new BigDecimal(48),18));
        foodMapCache.put(5, new Food(5,"红烧肉",new BigDecimal(58),30));
        foodMapCache.put(6, new Food(6,"鱼香肉丝",new BigDecimal(32),15));
    }
    public static void main(String[] args)  throws  Exception{
        List<Food> foods = new ArrayList<>();
        //饭店吃饭
       new Thread(new Runnable() {
            @Override
            public void run() {
                foods.add(foodMapCache.get(1));
                foods.add(foodMapCache.get(3));
                foods.add(foodMapCache.get(5));
                System.out.println("开始点菜");
                foods.forEach(food -> {
                    System.out.println("客官点了：" +food.getName());
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
                System.out.println("点菜完成");
            }
        }).start();



        //点完菜开始做饭
        Observable<List<Food>> backFoodObservable = Observable.from(new Future<List<Food>>() {
            List<Food> list = new ArrayList<>();
            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                return false;
            }

            @Override
            public boolean isCancelled() {
                return false;
            }

            @Override
            public boolean isDone() {
                return list.size() == foods.size() ;
            }

            @Override
            public List<Food> get() throws InterruptedException, ExecutionException {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        foods.forEach(food -> {
                            try {
                                Thread.sleep(food.getMakeTime());
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            list.add(food);
                            System.out.println("客官，"+food.getName()+"做好了。");
                        });
                    }
                }).start();
                return list;
            }

            @Override
            public List<Food> get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
                return null;
            }
        });

        Observable<String> mObservable = Observable.create(new Observable.OnSubscribe<String>() {
            int size = 0;
            @Override
            public void call(Subscriber<? super String> subscriber) {
                backFoodObservable.doOnNext(foods1 -> {
                    if(size !=  foods1.size()){
                        System.out.println(foods1.get(size));
                        size++;
                    }
                });
            }
        });






        //催菜（每个3s催一次）+ 反馈

        //上菜（10s后上菜）

        //吃（每道菜5s后吃完）

        //结束吃饭
    }
}
