package com.tangtang.basic.rxjava;

import rx.Observable;
import rx.functions.Func1;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

public class ObservableDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        System.out.println(Observable.fromCallable(new Callable<String>() {
            @Override
            public String call() throws Exception {
                Thread.sleep(5000);
                return "hello world";
            }
        }).filter(new Func1<String, Boolean>() {
            @Override
            public Boolean call(String s) {
                return s.contains("hello");
            }
        }).asObservable().toBlocking().toFuture().get());
    }
}
