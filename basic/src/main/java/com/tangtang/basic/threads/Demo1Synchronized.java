package com.tangtang.basic.threads;

public class Demo1Synchronized {
    private int i = 0;

    public static void main(String[] args) {
        Demo1Synchronized demo1 = new Demo1Synchronized();
      new Thread(  new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i <10 ; i++) {
                    System.out.println(Thread.currentThread().getName());
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    demo1.add();
                }
            }
        }).start();

        new Thread(  new Runnable() {
            @Override
            public void run() {

                for (int i = 0; i <10 ; i++) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    demo1.add();
                }
            }
        }).start();

    }

    public  synchronized void add(){
         i++;
        System.out.println(Thread.currentThread().getName()+"->"+i);
    }


}
