package com.tangtang.basic.threads.demo;

public class JoinDemo {
    public static void main(String[] args)  throws  Exception{

        Thread thread1 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println("线程1执行" + i);
                }
            }
        };

        Thread thread2 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println("线程2执行" + i);
                }
            }
        };

        thread1.start();
        thread1.join();
        thread2.start();
    }
}
