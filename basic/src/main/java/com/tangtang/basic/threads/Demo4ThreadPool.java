package com.tangtang.basic.threads;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Demo4ThreadPool {
    private AtomicInteger i = new AtomicInteger(0);

    public static void main(String[] args) {
        Demo4ThreadPool demo1 = new Demo4ThreadPool();
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(3, 5, 60L,  TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
//        ThreadPoolExecutor executor = new ThreadPoolExecutor(3, Integer.MAX_VALUE, 60, TimeUnit.MICROSECONDS, new LinkedBlockingDeque<>(), ThreadPoolExecutor.DiscardOldestPolicy.class);
        ExecutorService threadPool1 = Executors.newFixedThreadPool(10);
        Runnable task = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println(Thread.currentThread().getName());
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    demo1.add();
                }
            }
        };
        for (int i = 0; i < 10; i++) {
            threadPool.submit(task);
        }



    }

    public synchronized void add() {

        i.getAndAdd(1);

        System.out.println(i);
    }


}
