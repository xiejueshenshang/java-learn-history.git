package com.tangtang.basic.threads.demo;

public class InterruptDemo {
    public static void main(String[] args)  throws  Exception{

        Thread thread1 = new Thread() {
            @Override
            public void run() {
                while (true){

                    if(!this.isInterrupted()){
                        System.out.println("线程1执行");
                    }

                }
            }
        };
        thread1.start();

       Thread.sleep(1000);
        thread1.interrupt();
        System.out.println("线程1被暂停");
//
    }
}
