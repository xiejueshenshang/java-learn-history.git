package com.tangtang.basic.threads;

public class Demo5Volitile {
    private int i = 0;


    public static void main(String[] args) {
        Demo5Volitile demo1 = new Demo5Volitile();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(100L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                demo1.add();
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(100L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                demo1.add();
            }
        }).start();

    }

    public void add() {
        i++;
        System.out.println(Thread.currentThread().getName() + "->" + i);
    }

}
