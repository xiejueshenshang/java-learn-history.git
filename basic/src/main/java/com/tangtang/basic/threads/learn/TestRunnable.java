package com.tangtang.basic.threads.learn;

public class TestRunnable implements Runnable {
    @Override
    public void run() {
        for(int i=1;i<11;i++){
    		 System.out.println(Thread.currentThread().getName()+" thread "+ i);
        }
    }
    
    public static void main(String[] args) {
        Thread t1=new Thread(new TestRunnable(),"thread1");
        Thread t2=new Thread(new TestRunnable(),"thread2");
        Thread t3=new Thread(new TestRunnable(),"thread3");
        t1.start();
        t2.start();
        t3.start();
    }
}

