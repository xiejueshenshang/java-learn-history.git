package com.tangtang.basic.threads;

public class Demo5Volitile1 {
    private  static volatile boolean b = false;

    public boolean isB() {
        return b;
    }

    public static void main(String[] args) {
        Demo5Volitile1 demo1 = new Demo5Volitile1();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                b = true;
                System.out.println(Thread.currentThread().getName() + "->" + b);
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (!b){
                    System.out.println("pases");
                    try {
                        Thread.sleep(100L);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }

    public void set() {
        b = true;
        System.out.println(Thread.currentThread().getName() + "->" + b);
    }

}
