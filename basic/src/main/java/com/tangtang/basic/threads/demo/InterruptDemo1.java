package com.tangtang.basic.threads.demo;

public class InterruptDemo1 {
    public static void main(String[] args) throws Exception {

        MyThead myThead = new MyThead();

        myThead.start();

        Thread.sleep(3000);
        myThead.changeStatus();
        System.out.println("线程1被暂停");
    }

    public static class MyThead extends Thread {
        public boolean isRun = true;
        public void changeStatus(){
            isRun = false;
            this.interrupt();
        }



        @Override
        public void run() {
            while (isRun) {
                System.out.println(Thread.currentThread().getName() + "，正在运行");
                try {
                    Thread.sleep(1000 * 30);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }


    }
}
