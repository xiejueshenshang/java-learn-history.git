package com.tangtang.basic.threads.learn;

public class ThreadTest extends Thread {

    public void startThread() {
        Thread t = new Thread( new Runnable() {

            @Override
            public void run() {
                System.out.println( "开始执行线程...");
                System.out.println( "进入等待状态...");
                synchronized (this) {
                    try {
                        this.wait(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println( "线程结束...");
            }
        });
        t.start();


    }
    private String name;

    public ThreadTest(String name) {
        this.name = name;

    }

    @Override
    public void run() {
//        for (int i = 1; i < 11; i++) {
//            System.out.println(Thread.currentThread().getName() + " thread " + i);
//        }
        ThreadTest threadTest = new ThreadTest("");
        threadTest.startThread();

    }

    public static void main(String[] args) {
        ThreadTest t1 = new ThreadTest("thread1");
        ThreadTest t2 = new ThreadTest("thread2");
        ThreadTest t3 = new ThreadTest("thread3");
        t1.start();
        t2.start();
        t3.start();
    }


    class Account {
        public void draw(double drawAmount) {
            synchronized(this) {
                System.out.println("hello");
            }
        }
        public synchronized void drawTask(double drawAmount) {
            System.out.println("hello");
        }
    }



}
