package com.tangtang.basic.threads;

public class SynchronizedUnSafeDemo {
    private volatile  static  int data = 0;

    public static void main(String[] args)  throws  Exception{
        Thread thread1 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 20; i++) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    data++;
                    System.out.println(data);
                }
            }
        };
        thread1.start();
        Thread thread2 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 20; i++) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    data++;
                    System.out.println(data);
                }
            }
        };
        thread2.start();

        thread1.join();
        thread2.join();
    }
}
