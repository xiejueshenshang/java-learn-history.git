package com.tangtang.basic.proxy.jdkdemo;

import java.lang.reflect.Proxy;

public class TestJDKProxy {
    public static void main(String[] args) {
        UserService object = new UserServiceImpl();
        MyInvocationHandler myInvocationHandler = new MyInvocationHandler(object);
        UserService userService1 = (UserService) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                UserServiceImpl.class.getInterfaces(), myInvocationHandler);
        userService1.addUser("张三");

        userService1.findUserById(1L);

    }
}
