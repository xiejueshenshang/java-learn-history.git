package com.tangtang.basic.proxy.jdkdemo;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MyInvocationHandler implements InvocationHandler {

    private Object objects;

    public MyInvocationHandler(Object objects) {
        this.objects = objects;
    }

    public Object getObjects() {
        return objects;
    }

    public void setObjects(Object objects) {
        this.objects = objects;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("do something before");

        // 调用被代理对象的方法并得到返回值

        Object result = method.invoke(objects, args);

        System.out.println("do something after");

        return result;
    }


}
