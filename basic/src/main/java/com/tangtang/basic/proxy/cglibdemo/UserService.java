package com.tangtang.basic.proxy.cglibdemo;

public class UserService {
    public void addUser(String name) {
        System.out.println("新增了一个用户:" + name);
    }

    public String findUserById(Long id) {
        System.out.println("查询到了一个id为" + id + "的用户");
        return id.toString();
    }
}
