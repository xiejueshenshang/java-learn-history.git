package com.tangtang.basic.proxy.jdkdemo;

public interface UserService {
    void addUser(String name);

    String findUserById(Long id);

}
