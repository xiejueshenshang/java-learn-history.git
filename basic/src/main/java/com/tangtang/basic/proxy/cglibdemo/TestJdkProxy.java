package com.tangtang.basic.proxy.cglibdemo;

import com.tangtang.basic.proxy.jdkdemo.UserService;
import com.tangtang.basic.proxy.jdkdemo.UserServiceImpl;
import net.sf.cglib.proxy.Enhancer;

public class TestJdkProxy {
    public static void main(String[] args) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(UserServiceImpl.class);
        enhancer.setCallback(new MyMethodInterceptor());
        UserService userService = (UserServiceImpl) enhancer.create();
        userService.addUser("张三");
    }
}
