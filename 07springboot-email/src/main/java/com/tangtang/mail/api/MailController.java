package com.tangtang.mail.api;

//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiOperation;
import freemarker.template.utility.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import freemarker.template.Configuration;
import freemarker.template.Template;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Api("发票发送")
@AllArgsConstructor
@RequestMapping("api/mail")
@RestController
public class MailController {


    private JavaMailSender javaMailSender;

    private Configuration freemarkerConfig;

    @ApiOperation(value =  "helloworld",notes = "test helloworld")
    @GetMapping("hello")
    public String hello(){
        return "hello world";
    }


    @GetMapping("send")
    public Boolean sendEmail(String dataSerialNo)  throws  Exception{

        Template template = this.freemarkerConfig.getTemplate("invoice.html");
        Map<String, Object> model = new HashMap<>();
        model.put("companyName", "companyName");
        model.put("messageType", "电子发票");
        model.put("appName", "国网");
        model.put("messageTitle", "查收电子发票");
        model.put("messageUrl", "");
        final Date date = new Date();
        final String dateStr = "2020-01-01 00:00:00";
        final String messageAbstract;
        messageAbstract = String.format("您的供应商给您开具并发送了一张电子发票, 发票代码: %s, 发票号码：%s, 开票日期：%s ,校验码：%s, " +
                "请通过下方按钮或链接查看及下载", "发票好", "existEntity.getInvoiceNumber()", dateStr," existEntity.getCheckCode()");
        model.put("messageAbstract", messageAbstract);
        //生成模板数据
        String content = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
        //发送邮件
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, Boolean.TRUE);
        helper.setFrom("1179672922@qq.com");
        helper.setTo(new String[]{"15981339380@163.com"});
        helper.setSubject("电子发票");
        helper.setText(content, Boolean.TRUE);

        FileSystemResource file2 = new FileSystemResource(new File("/Users/tangweiming/Desktop/Project/java-learn-history/07springboot-email/src/main/java/com/tangtang/mail/MailApplication.java"));

        helper.addAttachment("附件-1.java", file2);
        helper.addAttachment("附件-2.java", file2);

        javaMailSender.send(mimeMessage);
        return true;
    }
}
