package com.tangtang.register.client;


import lombok.Data;

@Data
public class HeartbeatRespnce {

    public final static String SUCCESS = "SUCCESS";
    public final static String FAILURE = "FAILURE";

    private String status;

    private Integer code;
}
