package com.tangtang.register.client;

import lombok.Data;

@Data
public class RegisterRequest {
    private String serverName;

    private String ip;

    private String hostName;

    private int port;

    private String serviceInstenceId;


}
