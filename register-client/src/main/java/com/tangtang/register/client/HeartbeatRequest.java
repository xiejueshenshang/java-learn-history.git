package com.tangtang.register.client;


import lombok.Data;

@Data
public class HeartbeatRequest {
    private String serviceInstanceId;

    private String serviceName;

}
